1
00:00:00,720 --> 00:00:02,080
[Autogenerated] I don't know about you,

2
00:00:02,080 --> 00:00:04,790
but I feel a demo would be very welcomed

3
00:00:04,790 --> 00:00:07,670
right now. So let's go ahead and implement

4
00:00:07,670 --> 00:00:09,640
and execute Mila queries In spring

5
00:00:09,640 --> 00:00:12,990
applications, we will explore filtering

6
00:00:12,990 --> 00:00:15,610
ranging from simple to advanced was Take a

7
00:00:15,610 --> 00:00:18,640
look at sorting Beijing and finally will

8
00:00:18,640 --> 00:00:21,520
implement full tech search. So let's get

9
00:00:21,520 --> 00:00:24,980
to work before we can implement queries.

10
00:00:24,980 --> 00:00:27,450
We have toe add some information into the

11
00:00:27,450 --> 00:00:31,220
database, so to do liberty and created six

12
00:00:31,220 --> 00:00:33,550
documents in the airport management

13
00:00:33,550 --> 00:00:35,610
flights collection. So we have a couple of

14
00:00:35,610 --> 00:00:38,380
flights over here with various properties

15
00:00:38,380 --> 00:00:40,830
that will help us write enquiries.

16
00:00:40,830 --> 00:00:42,510
Awesome. Now we can start to write some

17
00:00:42,510 --> 00:00:44,870
coat. This is the application, as you left

18
00:00:44,870 --> 00:00:46,910
it in our previous module. I hope you

19
00:00:46,910 --> 00:00:49,390
remember it. Flight information was the

20
00:00:49,390 --> 00:00:51,860
main entity. It is the class that we're

21
00:00:51,860 --> 00:00:54,210
going to persist in mongo TB. Now, let's

22
00:00:54,210 --> 00:00:56,730
go ahead and implement some queries. I

23
00:00:56,730 --> 00:01:00,220
created this package parasite, airport

24
00:01:00,220 --> 00:01:02,620
management queries and I have this empty

25
00:01:02,620 --> 00:01:04,390
class over here. Flight information

26
00:01:04,390 --> 00:01:08,830
queries. Flight information queries is an

27
00:01:08,830 --> 00:01:11,290
outdated with at service because I want to

28
00:01:11,290 --> 00:01:14,310
inject it in our spring application. Now

29
00:01:14,310 --> 00:01:16,540
you can altitude with our component also,

30
00:01:16,540 --> 00:01:18,960
But I really believe that this class is

31
00:01:18,960 --> 00:01:20,880
really a service, So I wanted to make

32
00:01:20,880 --> 00:01:23,600
things as explicit as possible. The first

33
00:01:23,600 --> 00:01:25,490
thing that we need to do before you write

34
00:01:25,490 --> 00:01:27,740
any operation against the Mongol database

35
00:01:27,740 --> 00:01:30,330
ist injected the manga template. And this

36
00:01:30,330 --> 00:01:32,890
is what we did over here. Have a private

37
00:01:32,890 --> 00:01:35,520
Molitor pretty yield, which is injected

38
00:01:35,520 --> 00:01:37,880
via the constructor and then spring will

39
00:01:37,880 --> 00:01:40,640
take care of the rest. In the comments,

40
00:01:40,640 --> 00:01:42,970
you can see all the requirements that are

41
00:01:42,970 --> 00:01:45,600
expected of us. We have to build queries

42
00:01:45,600 --> 00:01:47,920
that tree of all the flights with Beijing

43
00:01:47,920 --> 00:01:50,690
and sorting. You have to retrieved flights

44
00:01:50,690 --> 00:01:52,740
by D. You have to count all the

45
00:01:52,740 --> 00:01:54,770
international sites. And then we have to

46
00:01:54,770 --> 00:01:57,040
provide methods to find all the flights by

47
00:01:57,040 --> 00:01:59,430
departure city, all the flight through the

48
00:01:59,430 --> 00:02:01,990
duration between them in a max, all the

49
00:02:01,990 --> 00:02:04,920
flights delayed at a particular airport,

50
00:02:04,920 --> 00:02:07,190
flights that are on time and that relate

51
00:02:07,190 --> 00:02:09,930
to a particular city and to find fights by

52
00:02:09,930 --> 00:02:13,140
aircraft module who have a lot to do. So

53
00:02:13,140 --> 00:02:15,950
let's get to work. I'm going to deliver a

54
00:02:15,950 --> 00:02:19,120
comment and rather queries using some

55
00:02:19,120 --> 00:02:22,230
snippet magic. Brilliant. Let's take a

56
00:02:22,230 --> 00:02:25,420
look at these queries we have find all

57
00:02:25,420 --> 00:02:28,240
over here we define the query. We name it

58
00:02:28,240 --> 00:02:31,860
all paged and ordered. And then we define

59
00:02:31,860 --> 00:02:34,380
an empty query. Remember, want to find all

60
00:02:34,380 --> 00:02:36,470
the flight information? So we do not have

61
00:02:36,470 --> 00:02:38,680
to provide criteria but you to provide

62
00:02:38,680 --> 00:02:42,280
sorting and imagination and, of course,

63
00:02:42,280 --> 00:02:44,460
used the Mongol template we call the fine

64
00:02:44,460 --> 00:02:46,580
method personal query and the flight

65
00:02:46,580 --> 00:02:49,310
information. That class type find single

66
00:02:49,310 --> 00:02:52,280
bidi is simpler because we just have to

67
00:02:52,280 --> 00:02:55,420
delegate the coal to find by any method on

68
00:02:55,420 --> 00:02:57,770
the Mongol template. Then we need to count

69
00:02:57,770 --> 00:02:59,640
all the international flights and return

70
00:02:59,640 --> 00:03:02,410
their number. We're building a new query

71
00:03:02,410 --> 00:03:05,840
we add in this criteria where type is

72
00:03:05,840 --> 00:03:08,540
international, and then we're using the

73
00:03:08,540 --> 00:03:11,310
current method on the mobile temple class

74
00:03:11,310 --> 00:03:14,480
to retrieve that number. We can also find

75
00:03:14,480 --> 00:03:16,700
flights by departure. We create a new

76
00:03:16,700 --> 00:03:18,880
query. We call it by departure, and we add

77
00:03:18,880 --> 00:03:21,790
in this criteria where departure is

78
00:03:21,790 --> 00:03:24,070
departure, the string that we will receive

79
00:03:24,070 --> 00:03:26,850
in his method and then we delegated to the

80
00:03:26,850 --> 00:03:29,320
fine method on the model of the class.

81
00:03:29,320 --> 00:03:30,760
There's go to something a little bit more

82
00:03:30,760 --> 00:03:33,380
complex. Let's find all the flights that

83
00:03:33,380 --> 00:03:35,520
have a duration between a mean and the max

84
00:03:35,520 --> 00:03:38,400
value. The greatest query over here by

85
00:03:38,400 --> 00:03:41,300
duration between, we add in the criteria

86
00:03:41,300 --> 00:03:43,960
on the duration mean field. And then we

87
00:03:43,960 --> 00:03:46,610
change together two operators greater than

88
00:03:46,610 --> 00:03:49,650
or equals and then less than equals every

89
00:03:49,650 --> 00:03:51,210
passing the mean and the max number of

90
00:03:51,210 --> 00:03:53,650
minutes. And then we are sorting his

91
00:03:53,650 --> 00:03:57,500
result in a descending order by the flight

92
00:03:57,500 --> 00:04:00,410
duration off course, we pass this to the

93
00:04:00,410 --> 00:04:02,520
Mongol template, find method and that's

94
00:04:02,520 --> 00:04:05,040
it. If you want to find all the delayed

95
00:04:05,040 --> 00:04:09,010
flights at a particular departure, we can

96
00:04:09,010 --> 00:04:11,970
use this quarry delayed a departure. We

97
00:04:11,970 --> 00:04:15,330
have two criteria. The first criteria is

98
00:04:15,330 --> 00:04:17,450
to fill their flights that are delayed. We

99
00:04:17,450 --> 00:04:19,930
have this over here where delayed is true,

100
00:04:19,930 --> 00:04:22,290
and then we can use the second criteria.

101
00:04:22,290 --> 00:04:26,710
Using the end method and departure is the

102
00:04:26,710 --> 00:04:28,770
departure city, and this is how we can

103
00:04:28,770 --> 00:04:31,950
chain toe criteria together. Let's find

104
00:04:31,950 --> 00:04:34,720
all the fights related to a city that that

105
00:04:34,720 --> 00:04:37,180
are not delayed. Here. Things get really

106
00:04:37,180 --> 00:04:38,710
interesting because you have to use our

107
00:04:38,710 --> 00:04:42,400
operators and end operators so it gives

108
00:04:42,400 --> 00:04:45,440
our query and we add in this criteria in

109
00:04:45,440 --> 00:04:47,550
the operator, we want to fill their

110
00:04:47,550 --> 00:04:50,020
flights that relates to a given city. So

111
00:04:50,020 --> 00:04:52,320
where the destination or departure is

112
00:04:52,320 --> 00:04:55,640
equal to that city And then I also want to

113
00:04:55,640 --> 00:04:57,620
restrict the slights and make sure that

114
00:04:57,620 --> 00:04:59,710
they're not the late. That's why we use

115
00:04:59,710 --> 00:05:02,140
the end operator over here. A passing the

116
00:05:02,140 --> 00:05:05,370
appropriate criteria. We can see how easy

117
00:05:05,370 --> 00:05:07,830
it is to build queries using this fluent

118
00:05:07,830 --> 00:05:10,760
interface. They all follow the same recipe

119
00:05:10,760 --> 00:05:12,920
unless banalities you want to find all the

120
00:05:12,920 --> 00:05:14,790
fat information by aircraft. So again,

121
00:05:14,790 --> 00:05:16,760
rebuilding a criteria. And look at the

122
00:05:16,760 --> 00:05:19,870
syntax over here where aircraft that model

123
00:05:19,870 --> 00:05:22,920
is aircraft aircraft is a property of

124
00:05:22,920 --> 00:05:25,030
flight information and model is a property

125
00:05:25,030 --> 00:05:28,000
on aircraft. And remember, we can perform

126
00:05:28,000 --> 00:05:30,660
queries in sub documents. I want to

127
00:05:30,660 --> 00:05:32,830
execute all queries using the application

128
00:05:32,830 --> 00:05:34,570
Rommel class. This class implements the

129
00:05:34,570 --> 00:05:36,440
command line runner, and if you remember

130
00:05:36,440 --> 00:05:38,390
from the previous module, Camaro and

131
00:05:38,390 --> 00:05:40,380
runners are executed after a spring

132
00:05:40,380 --> 00:05:42,570
application is bootstrapped. We have a

133
00:05:42,570 --> 00:05:44,960
dependency on flight information that will

134
00:05:44,960 --> 00:05:46,980
inject together constructor, and then we

135
00:05:46,980 --> 00:05:49,270
have this run method over here. So again,

136
00:05:49,270 --> 00:05:51,740
we're going to use some intelligent e

137
00:05:51,740 --> 00:05:54,490
snippet magic, and we're going to run all

138
00:05:54,490 --> 00:05:56,890
of our queries and they look the same from

139
00:05:56,890 --> 00:05:59,810
we're printing the query that execute. And

140
00:05:59,810 --> 00:06:02,880
then we are calling every method on the

141
00:06:02,880 --> 00:06:04,880
flight information Cory Service and then

142
00:06:04,880 --> 00:06:07,530
reaching the flight printer class to print

143
00:06:07,530 --> 00:06:10,510
the result for every query that we have

144
00:06:10,510 --> 00:06:13,060
defined. So go ahead. We run the

145
00:06:13,060 --> 00:06:16,110
application and it's all come that we get

146
00:06:16,110 --> 00:06:18,530
and we have all the results displayed here

147
00:06:18,530 --> 00:06:19,830
and let's let's take a look at the

148
00:06:19,830 --> 00:06:21,580
particular quality a part of New York can

149
00:06:21,580 --> 00:06:24,370
see that only one result displayed. Then

150
00:06:24,370 --> 00:06:26,740
we have delayed departures as given

151
00:06:26,740 --> 00:06:28,650
airport and the airport is Madrid. So the

152
00:06:28,650 --> 00:06:30,630
State Department red. And we also see that

153
00:06:30,630 --> 00:06:32,890
the flight is delayed. It appears that all

154
00:06:32,890 --> 00:06:34,750
of our careers have worked successfully,

155
00:06:34,750 --> 00:06:38,110
which is just amazing. I don't know about

156
00:06:38,110 --> 00:06:40,460
you, but I'm very eager to implement a

157
00:06:40,460 --> 00:06:42,860
full tech search. The first thing that we

158
00:06:42,860 --> 00:06:44,770
need to do to implement food tech search

159
00:06:44,770 --> 00:06:47,250
is to use the annotations, the at text

160
00:06:47,250 --> 00:06:50,060
indexed annotations as a deliberate Ito

161
00:06:50,060 --> 00:06:52,790
Adam on the departure city. This nation,

162
00:06:52,790 --> 00:06:56,860
city and description not is that the text

163
00:06:56,860 --> 00:06:59,350
index on the description has a weight off

164
00:06:59,350 --> 00:07:01,630
to We shall see how this affect the search

165
00:07:01,630 --> 00:07:05,330
results later on. Now let's go back of the

166
00:07:05,330 --> 00:07:07,850
flight information, quits class and adding

167
00:07:07,850 --> 00:07:10,120
a new worry for free tech search. And

168
00:07:10,120 --> 00:07:12,780
let's take a good look at his method. This

169
00:07:12,780 --> 00:07:15,530
method receives a string call text does

170
00:07:15,530 --> 00:07:17,960
the free text by which you want to perform

171
00:07:17,960 --> 00:07:20,740
the search. Then we define or text

172
00:07:20,740 --> 00:07:23,700
criteria a special type off criteria. This

173
00:07:23,700 --> 00:07:26,880
criteria depends on the language in which

174
00:07:26,880 --> 00:07:28,420
you want to perform the search. In our

175
00:07:28,420 --> 00:07:30,310
case, we just use the default system

176
00:07:30,310 --> 00:07:32,820
language and then we're going to match

177
00:07:32,820 --> 00:07:35,280
against the given text. Not that we have

178
00:07:35,280 --> 00:07:36,920
our text criteria. Just go ahead and build

179
00:07:36,920 --> 00:07:39,740
a query will name it by free text, and

180
00:07:39,740 --> 00:07:42,360
we're going to create it by using text

181
00:07:42,360 --> 00:07:45,280
query that pretext method. And then we're

182
00:07:45,280 --> 00:07:47,610
going to pass in the text bacteria. We're

183
00:07:47,610 --> 00:07:49,750
going to sort the results by the score.

184
00:07:49,750 --> 00:07:52,880
And the score is the internal score that

185
00:07:52,880 --> 00:07:54,850
Mangal will compute for the free tech

186
00:07:54,850 --> 00:07:56,990
search based on the weights of the text in

187
00:07:56,990 --> 00:07:59,190
Texas. And then we're going to patch in it

188
00:07:59,190 --> 00:08:01,590
this and return the first page with free

189
00:08:01,590 --> 00:08:03,200
results. We're going to pass this to the

190
00:08:03,200 --> 00:08:05,620
Mongol template and executed. The last

191
00:08:05,620 --> 00:08:07,790
piece of the puzzle is to actually execute

192
00:08:07,790 --> 00:08:09,520
that coat we're going toe, print all the

193
00:08:09,520 --> 00:08:11,260
flights, and then we're going to call the

194
00:08:11,260 --> 00:08:13,830
fine by free text method and pass in the

195
00:08:13,830 --> 00:08:16,710
text off room. Let's run the application

196
00:08:16,710 --> 00:08:19,380
and see what's happening, and we have our

197
00:08:19,380 --> 00:08:20,990
results. All the fights are displayed

198
00:08:20,990 --> 00:08:23,160
above and are free text. Search my room,

199
00:08:23,160 --> 00:08:25,870
which is paid below. Look at the order of

200
00:08:25,870 --> 00:08:28,140
these documents. The 1st 1 is the flight

201
00:08:28,140 --> 00:08:30,050
from Rome to Paris is the 1st 1 because it

202
00:08:30,050 --> 00:08:32,510
contains room both in departure and the

203
00:08:32,510 --> 00:08:35,640
description. The 2nd 1 is New York to

204
00:08:35,640 --> 00:08:38,250
Copenhagen, and it was selected because it

205
00:08:38,250 --> 00:08:41,070
contains Rome in the description.

206
00:08:41,070 --> 00:08:42,930
Remember, that description has a bigger

207
00:08:42,930 --> 00:08:45,390
weight, and the last point is Bucharest to

208
00:08:45,390 --> 00:08:47,950
Rome and as the last one, because it only

209
00:08:47,950 --> 00:08:50,140
contains room the destination. Now, of

210
00:08:50,140 --> 00:08:52,330
course, you might argue that the way we

211
00:08:52,330 --> 00:08:54,460
created the weights is not correct,

212
00:08:54,460 --> 00:08:56,620
because we would expect that bequest to

213
00:08:56,620 --> 00:08:59,530
wrong to be situated above your to

214
00:08:59,530 --> 00:09:01,730
Copenhagen. But that's exactly what I

215
00:09:01,730 --> 00:09:07,000
wanted to point out the importance of weights when you're using text indexes

