1. Course Introduction
   
    -  Pre-requisites:
        - Comfortable writing Java application
        - Basic experience using Spring and Spring Boot
        - Some familiarity with MongoDB
    - Get up and running:
        - Prepare development environment:
          - Java 8+
          - MongoDB
          - IDE
          - Mongo GUI:
            - Robo 3T
            - Mongo Compass
        - Create an empty Spring Boot project
        - Add the Spring Data MongoDB starter dependency
        - Define the connection properties
        - Apply Mongo data annotation to control persistence
   
2. Basic MongoDB Concepts
    
    - skip
    
3. Spring Mongo Dependencies

    - Add Spring Data MongoDB dependency
    ```
   <dependencies>
        ...

        <!-- Spring Data MongoDB -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-mongodb</artifactId>
        </dependency>
    </dependencies>
    ```
   
        - This dependency include:
            - mongodb-driver
                - mongodb-driver: bson
                - mongodb-driver: driver-core
            - spring-data-mongodb
                - spring-data-commons
   
4. Spring Mongo Data Annotations
   
    - Most used annotations:
        - @Document
        - @Id
        - @Field
          ```java
          @Document(collection="airplanes")
          public class Aircraft {
            @Id private String id;
            private String model;
            @Field(name="seats") private int nbSeats; 
          }
          ```
      
        - @Transient
        
        ```java
          @Document(collection="airplanes")
          public class Aircraft {
            @Id private String id;
            private String model;
            @Transient private int nbSeats; 
          }
        ```
      
        - @Indexed
        ```java
          @Document(collection="airplanes")
          public class Aircraft {
            @Id private String id;
            @Indexed(direction=IndexDirection.ASCENDING, unique=false) 
            private String model;
            private int nbSeats; 
          }
        ```
        - @TextIndexed
        ```java
          @Document(collection="airplanes")
          public class Aircraft {
            @Id private String id;
            @TextIndexed private String model;
            private int nbSeats; 
          }
        ```
        - @CompoundIndex
        ```java
          @Document(collection="airplanes")
          @CompoundIndex(name = "model_seat", def="{'model': 1, 'nbSeats': -1}")
          public class Aircraft {
            @Id private String id;
            @TextIndexed private String model;
            private int nbSeats; 
          }
        ```
      
        - @CompoundIndexes
      ```java
          @Document(collection="airplanes")
          @CompoundIndexes({
            @CompoundIndex(name = "model_seat", def="{'model': 1, 'nbSeats': -1}")
          })
          public class Aircraft {
            @Id private String id;
            @TextIndexed private String model;
            private int nbSeats; 
          }
       ```
      
        - @DbRef: link or join data
        ```java
          @Document
          public class Manufacture {
            @Id private String id;
            private String name; 
          }
      
          @Document
          public class Aircraft {
          @Id private String id;
          private String model;    
            
          @DbRef Manufacture man;
          }
        ```
    
5. Mongo Connection Properties

- Basic Connection Properties
    - Mongo Server Ip address
    - Mongo Server Port
    - Database Name
    - Credentials [ optional ]
- Via Properties File
    ```
    spring.data.mongodb.host=localhost
    spring.data.mongodb.port=27017
    spring.data.mongodb.database=airportdb
    spring.data.mongodb.username=admin
    spring.data.mongodb.password=pass1234
    ```
    or use mongo URI
    ```
    spring.data.mongodb.uri=mongodb://admin:pass123@localhost:27017/airportdb
    ```
- At runtime    
```bash
java -jar ./myapp.jar 
--spring.data.mongodb.uri=mongodb://admin:pass123@localhost:27017/airportdb
```

6. Demo - Connecting a Spring Application to a MongoDB 
   
- 
7. Recap
