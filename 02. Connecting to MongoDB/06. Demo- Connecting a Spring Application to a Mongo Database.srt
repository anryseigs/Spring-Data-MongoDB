1
00:00:01,020 --> 00:00:02,000
[Autogenerated] it's time to get our hands

2
00:00:02,000 --> 00:00:04,380
dirty and write some code. In his demo,

3
00:00:04,380 --> 00:00:06,430
we're start by creating a simple spring

4
00:00:06,430 --> 00:00:08,230
with application and connected to a mongo

5
00:00:08,230 --> 00:00:11,510
database for import the new dependencies.

6
00:00:11,510 --> 00:00:13,470
Then we'll have the connection. Parameters

7
00:00:13,470 --> 00:00:15,560
will apply the annotations that we just

8
00:00:15,560 --> 00:00:18,050
learned, and hopefully our application

9
00:00:18,050 --> 00:00:20,180
should connect to an existing Manco

10
00:00:20,180 --> 00:00:23,300
server. Let's write some coat. Where do we

11
00:00:23,300 --> 00:00:25,580
go? We want to create a brand new spring

12
00:00:25,580 --> 00:00:28,560
application well, to start that spring

13
00:00:28,560 --> 00:00:31,830
that I'll I selected a maid from project

14
00:00:31,830 --> 00:00:34,120
our languages. Javon. The spring with

15
00:00:34,120 --> 00:00:35,860
version is 2 to 1 of which is the latest

16
00:00:35,860 --> 00:00:38,550
version up until today, and I want to

17
00:00:38,550 --> 00:00:40,650
create a project. The group would be pro

18
00:00:40,650 --> 00:00:43,100
site, and the artifact will be airport

19
00:00:43,100 --> 00:00:46,270
management actually generate and we'll

20
00:00:46,270 --> 00:00:49,060
have ah, brand new empty spring with

21
00:00:49,060 --> 00:00:51,300
application that can start to work

22
00:00:51,300 --> 00:00:53,840
against. I took a generated project, and

23
00:00:53,840 --> 00:00:56,250
I've opened it in intelligent. As you can

24
00:00:56,250 --> 00:00:58,640
see right here, we just have the bare

25
00:00:58,640 --> 00:01:00,710
minimum dependencies. Basically, we have a

26
00:01:00,710 --> 00:01:03,050
dependency on spring starter and spring

27
00:01:03,050 --> 00:01:05,680
with starter test. I also took the Liberty

28
00:01:05,680 --> 00:01:07,340
to create a couple of classes to speed

29
00:01:07,340 --> 00:01:09,230
things up a little bit the airport

30
00:01:09,230 --> 00:01:11,350
management application class, his

31
00:01:11,350 --> 00:01:13,540
annotated with US promote application and

32
00:01:13,540 --> 00:01:16,630
is the spring application and two point it

33
00:01:16,630 --> 00:01:18,270
read around the spring application around

34
00:01:18,270 --> 00:01:21,620
method. I've also created this class

35
00:01:21,620 --> 00:01:23,020
called Application Runner, which

36
00:01:23,020 --> 00:01:24,660
implements the command line runner

37
00:01:24,660 --> 00:01:28,180
interface. This exposes the run method his

38
00:01:28,180 --> 00:01:30,550
spring. Every class that implements the

39
00:01:30,550 --> 00:01:32,750
command Liar, honor interface will get

40
00:01:32,750 --> 00:01:34,710
executed automatically after the

41
00:01:34,710 --> 00:01:37,370
application bootstraps. And I want to use

42
00:01:37,370 --> 00:01:40,030
this kind of concept to write our use

43
00:01:40,030 --> 00:01:42,850
cases. Then we have our domain and you

44
00:01:42,850 --> 00:01:45,410
want to manage flight information. So I've

45
00:01:45,410 --> 00:01:47,660
created a couple of classes. The 1st 1 is

46
00:01:47,660 --> 00:01:50,170
aircraft in. This class has two properties

47
00:01:50,170 --> 00:01:52,600
Ah, model and number of seats along with

48
00:01:52,600 --> 00:01:55,770
some Gethers. Then we have flight type,

49
00:01:55,770 --> 00:01:57,490
which is an imam, internal or

50
00:01:57,490 --> 00:01:59,990
international, And then we have our main

51
00:01:59,990 --> 00:02:02,390
entity, which is flight information. Now,

52
00:02:02,390 --> 00:02:04,120
flight information contains various

53
00:02:04,120 --> 00:02:06,490
properties that are of interest for people

54
00:02:06,490 --> 00:02:08,620
working in airport management. We have an

55
00:02:08,620 --> 00:02:11,120
idea. We have departure and destination

56
00:02:11,120 --> 00:02:13,860
the flight type. If the flight is delayed,

57
00:02:13,860 --> 00:02:15,860
the duration of the flight, the departure

58
00:02:15,860 --> 00:02:20,100
date, the aircraft and we also have some

59
00:02:20,100 --> 00:02:22,340
or did information here created at so when

60
00:02:22,340 --> 00:02:24,450
this flight information was created in our

61
00:02:24,450 --> 00:02:27,440
system, Then we have a stomach constructor

62
00:02:27,440 --> 00:02:29,760
and a bunch of getters and centers. So

63
00:02:29,760 --> 00:02:33,080
pretty standard stuff. We are now ready to

64
00:02:33,080 --> 00:02:35,540
connect its application to a mongo server

65
00:02:35,540 --> 00:02:38,000
on Let's Apply the principles in the steps

66
00:02:38,000 --> 00:02:40,420
that we discussed earlier on in his

67
00:02:40,420 --> 00:02:42,240
module. The first thing that you need to

68
00:02:42,240 --> 00:02:45,570
do is you two other dependency for spring

69
00:02:45,570 --> 00:02:49,520
daytime. I'm gonna be so go ahead, opened

70
00:02:49,520 --> 00:02:52,520
upon not xml file. And we're adding a

71
00:02:52,520 --> 00:02:55,390
brand new dependency, a dependency on

72
00:02:55,390 --> 00:02:59,040
spring boots, starter data, manga TV.

73
00:02:59,040 --> 00:03:01,750
We'll kick save. And now we have all the

74
00:03:01,750 --> 00:03:03,860
dependencies needed toe work with mongo to

75
00:03:03,860 --> 00:03:05,920
be. The next step is to add the

76
00:03:05,920 --> 00:03:07,730
appropriate annotations to the flight

77
00:03:07,730 --> 00:03:10,880
information class. Let's do that. Flight

78
00:03:10,880 --> 00:03:13,320
information is our main entity and wantto

79
00:03:13,320 --> 00:03:15,930
persisted as a monk a document. Therefore,

80
00:03:15,930 --> 00:03:19,100
we annotate this class with a document. I

81
00:03:19,100 --> 00:03:20,690
also want to change the name of the

82
00:03:20,690 --> 00:03:22,820
collection. I want to save all flight

83
00:03:22,820 --> 00:03:25,990
information in a collection called Fights.

84
00:03:25,990 --> 00:03:28,790
Then I have marked the idea property as

85
00:03:28,790 --> 00:03:31,260
the I D field off this flight information

86
00:03:31,260 --> 00:03:34,690
class departure city and this nation city

87
00:03:34,690 --> 00:03:36,590
have also been modified. I think their

88
00:03:36,590 --> 00:03:39,280
property names are a bit along. Therefore,

89
00:03:39,280 --> 00:03:41,090
I have renamed them to departure, and

90
00:03:41,090 --> 00:03:42,840
destination was in the at field

91
00:03:42,840 --> 00:03:45,560
annotation. I also an update them with at

92
00:03:45,560 --> 00:03:47,940
Indexed because we'll probably do a lot of

93
00:03:47,940 --> 00:03:50,050
filtering around departure and

94
00:03:50,050 --> 00:03:52,730
destination. And finally, I have marked

95
00:03:52,730 --> 00:03:55,330
the created at field with a transient

96
00:03:55,330 --> 00:03:58,080
created that is an audit information. We

97
00:03:58,080 --> 00:04:00,030
need it in our program, but you do not

98
00:04:00,030 --> 00:04:02,460
want to persist it in Mongo. This object

99
00:04:02,460 --> 00:04:04,600
is not ready to be saved in a mongo that

100
00:04:04,600 --> 00:04:07,080
always I just opened up Mongo Debbie

101
00:04:07,080 --> 00:04:09,650
Campus, a free graphical interface that

102
00:04:09,650 --> 00:04:11,620
helps you to navigate Mongo dippy

103
00:04:11,620 --> 00:04:13,670
databases and execute queries against

104
00:04:13,670 --> 00:04:15,820
them. My Mongol instance isn't star

105
00:04:15,820 --> 00:04:20,430
locally on local host Port 27017 That's

106
00:04:20,430 --> 00:04:23,120
where we need to connect. The server is

107
00:04:23,120 --> 00:04:24,820
not protected by credentials because it

108
00:04:24,820 --> 00:04:26,880
wants on. My local developed machine. Lets

109
00:04:26,880 --> 00:04:28,670
out this information in the Application

110
00:04:28,670 --> 00:04:30,230
Properties file. The Application

111
00:04:30,230 --> 00:04:32,730
Properties file is located here. Under the

112
00:04:32,730 --> 00:04:34,880
resource is Father. Let's write the

113
00:04:34,880 --> 00:04:37,200
necessary properties needs to connect to

114
00:04:37,200 --> 00:04:39,640
Mongo to be we have local host as the

115
00:04:39,640 --> 00:04:43,360
server. Port 27017 which is the default

116
00:04:43,360 --> 00:04:46,280
port and the database, which is airport

117
00:04:46,280 --> 00:04:48,860
management. We don't have any security on

118
00:04:48,860 --> 00:04:50,930
his database, so we don't need to pass in

119
00:04:50,930 --> 00:04:53,770
credentials. We can also use the shorthand

120
00:04:53,770 --> 00:04:55,890
version and get rid of the properties

121
00:04:55,890 --> 00:04:59,470
above. We can use spring data Mongol de

122
00:04:59,470 --> 00:05:03,710
Bure I and embed everything over here. So

123
00:05:03,710 --> 00:05:06,540
let's go ahead and start. Its application

124
00:05:06,540 --> 00:05:08,240
application is running and I want to

125
00:05:08,240 --> 00:05:10,630
highlight his line over here. Opened

126
00:05:10,630 --> 00:05:15,240
connection to local host Pour 27017 This

127
00:05:15,240 --> 00:05:17,660
is a success message telling us that we

128
00:05:17,660 --> 00:05:20,320
have established a connection toe among

129
00:05:20,320 --> 00:05:22,930
were to be severed. At this point, I could

130
00:05:22,930 --> 00:05:25,460
have entered the demo. However, I can help

131
00:05:25,460 --> 00:05:27,690
myself from wanting to write something in

132
00:05:27,690 --> 00:05:30,660
this mango database, So let's do that. I

133
00:05:30,660 --> 00:05:33,340
opened up the application runner class and

134
00:05:33,340 --> 00:05:35,770
I want to modify it to insert something in

135
00:05:35,770 --> 00:05:38,280
Margo. I want to insert an empty flight. I

136
00:05:38,280 --> 00:05:40,120
replaced the content, and now our

137
00:05:40,120 --> 00:05:42,880
application runner looks like this. We

138
00:05:42,880 --> 00:05:45,260
want to inject Margo template in the

139
00:05:45,260 --> 00:05:47,350
application. Rather, constructor mogul

140
00:05:47,350 --> 00:05:49,410
template is like the main class that

141
00:05:49,410 --> 00:05:52,410
executes query against databases. More on

142
00:05:52,410 --> 00:05:55,290
that in the upcoming modules, then in the

143
00:05:55,290 --> 00:05:58,120
run method. I created an empty flight, and

144
00:05:58,120 --> 00:05:59,990
I used the safe method on the Mogul

145
00:05:59,990 --> 00:06:03,370
template to persist it. Let's go ahead,

146
00:06:03,370 --> 00:06:06,220
start the application and welcome. Going

147
00:06:06,220 --> 00:06:07,990
back to Mongo Compass, we can see that a

148
00:06:07,990 --> 00:06:10,140
new database has been created. Airport

149
00:06:10,140 --> 00:06:13,220
management Airport management contains the

150
00:06:13,220 --> 00:06:16,830
collection in flights, and his collection

151
00:06:16,830 --> 00:06:19,810
most likely has the document that we just

152
00:06:19,810 --> 00:06:22,560
inserted. And here it is. You have this

153
00:06:22,560 --> 00:06:25,540
document with an auto generated I D and

154
00:06:25,540 --> 00:06:28,020
some basic information. So what just

155
00:06:28,020 --> 00:06:30,340
happened? Well behind the seas, when we

156
00:06:30,340 --> 00:06:31,930
used Margaret Template and the safe

157
00:06:31,930 --> 00:06:35,230
method, it automatic generated a database

158
00:06:35,230 --> 00:06:37,790
for us, the fight collection, and then it

159
00:06:37,790 --> 00:06:40,230
inserted that empty flight into the

160
00:06:40,230 --> 00:06:43,660
database. If you already had an existing

161
00:06:43,660 --> 00:06:45,840
database or collection, those would have

162
00:06:45,840 --> 00:06:48,320
been used. Instead, however, you can see

163
00:06:48,320 --> 00:06:50,540
the power and all the things that the

164
00:06:50,540 --> 00:06:57,000
Spring Data Mongo Debbie library Thus for us, I think it's pretty amazing

