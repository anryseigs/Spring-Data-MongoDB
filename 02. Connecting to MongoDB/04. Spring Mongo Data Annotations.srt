1
00:00:01,490 --> 00:00:02,960
[Autogenerated] Let's discuss about spring

2
00:00:02,960 --> 00:00:05,980
Mongol data. Annotations, data annotations

3
00:00:05,980 --> 00:00:07,950
are meta data that you cannot toe our

4
00:00:07,950 --> 00:00:10,790
classes or feels to let spring date. I

5
00:00:10,790 --> 00:00:13,930
know howto control the persistence in a

6
00:00:13,930 --> 00:00:17,390
mongo database. If you've ever worked a GP

7
00:00:17,390 --> 00:00:19,620
before the news, annotations will fear

8
00:00:19,620 --> 00:00:22,040
very, very familiar. And that's because

9
00:00:22,040 --> 00:00:24,340
the people working on the spring framework

10
00:00:24,340 --> 00:00:26,330
have done a great job in keeping the

11
00:00:26,330 --> 00:00:28,820
learning curve the minimal. And you're

12
00:00:28,820 --> 00:00:31,420
using some of the terminology. I want to

13
00:00:31,420 --> 00:00:34,780
focus on the most important mongo D B

14
00:00:34,780 --> 00:00:37,820
annotations, the ones that you'd use in

15
00:00:37,820 --> 00:00:45,330
95% of cases we have at a document at I. D

16
00:00:45,330 --> 00:00:49,440
at Field at Transient and wait. There's

17
00:00:49,440 --> 00:00:54,130
more we also have at indexed at Texting

18
00:00:54,130 --> 00:00:59,510
next at compounding Dex and at the B ref

19
00:00:59,510 --> 00:01:01,850
this. Take a closer look at them and get

20
00:01:01,850 --> 00:01:05,150
to know them in detail. Let's start with

21
00:01:05,150 --> 00:01:07,400
that document. A document is one of the

22
00:01:07,400 --> 00:01:09,230
most used annotations we're working with

23
00:01:09,230 --> 00:01:12,870
Mongo. Maybe we can use it on classes, and

24
00:01:12,870 --> 00:01:15,490
it lets the Mongol driver know that it can

25
00:01:15,490 --> 00:01:17,860
save objects of type aircraft as Mongol

26
00:01:17,860 --> 00:01:20,590
documents in a particular collection. In

27
00:01:20,590 --> 00:01:22,790
fact, it is very similar with the at

28
00:01:22,790 --> 00:01:25,420
entity annotation used by GPS when you're

29
00:01:25,420 --> 00:01:28,060
working with relational databases. But

30
00:01:28,060 --> 00:01:29,830
what is the name of the collection that

31
00:01:29,830 --> 00:01:32,180
these documents will be saved into? Well,

32
00:01:32,180 --> 00:01:34,300
if you was a document like this in the

33
00:01:34,300 --> 00:01:36,160
name of the collection is deduced from the

34
00:01:36,160 --> 00:01:39,720
class name in our case aircraft. However,

35
00:01:39,720 --> 00:01:42,170
we can also specify the collection name by

36
00:01:42,170 --> 00:01:44,020
using the collection attributes on this

37
00:01:44,020 --> 00:01:46,820
annotation. In this case, objects of type

38
00:01:46,820 --> 00:01:48,830
aircraft will be saved in a collection

39
00:01:48,830 --> 00:01:51,750
called her Place. Very useful when you

40
00:01:51,750 --> 00:01:53,780
want to control the collection name or

41
00:01:53,780 --> 00:01:56,970
when you want to get data on existing

42
00:01:56,970 --> 00:01:59,380
databases that have different names than

43
00:01:59,380 --> 00:02:02,040
the ones in your classes and another case,

44
00:02:02,040 --> 00:02:03,900
it's a very good practice to separate the

45
00:02:03,900 --> 00:02:07,140
name of her classes from the actual name

46
00:02:07,140 --> 00:02:10,210
off your collections. Every document in a

47
00:02:10,210 --> 00:02:12,780
mongo collection should have an I. D

48
00:02:12,780 --> 00:02:15,390
field. This idea field is unique, and it

49
00:02:15,390 --> 00:02:17,820
is indexed. It's very similar to a primary

50
00:02:17,820 --> 00:02:20,560
key in traditional databases. In fact, in

51
00:02:20,560 --> 00:02:23,390
GP, we have the exact same meditation on

52
00:02:23,390 --> 00:02:27,980
entities at I D. Now decided feels in a

53
00:02:27,980 --> 00:02:31,090
Mongol. Databases are usually stored as an

54
00:02:31,090 --> 00:02:34,920
object I deem type and this type resembles

55
00:02:34,920 --> 00:02:37,900
good. So when you're serializing and issue

56
00:02:37,900 --> 00:02:40,740
rising objects from among our database,

57
00:02:40,740 --> 00:02:44,500
your I d feels will probably be goods now

58
00:02:44,500 --> 00:02:46,700
that without defined an I D. What happens

59
00:02:46,700 --> 00:02:48,550
with model a number of seats, which are

60
00:02:48,550 --> 00:02:51,220
properties off the aircraft class, they

61
00:02:51,220 --> 00:02:53,000
will automatically get serialized.

62
00:02:53,000 --> 00:02:54,950
However, if you want to control that's

63
00:02:54,950 --> 00:02:57,400
realization, we can use the at field

64
00:02:57,400 --> 00:03:00,080
annotation as Field is very similar to a

65
00:03:00,080 --> 00:03:02,460
document but works on the property level.

66
00:03:02,460 --> 00:03:05,560
It instructs the framework on howto

67
00:03:05,560 --> 00:03:09,850
persist properties in our objects as feels

68
00:03:09,850 --> 00:03:12,610
in Mongol documents. And if you feel like

69
00:03:12,610 --> 00:03:14,220
this, it won't make any difference.

70
00:03:14,220 --> 00:03:17,140
However, we can use the name attribute on

71
00:03:17,140 --> 00:03:19,560
the ATF field annotation to control the

72
00:03:19,560 --> 00:03:22,270
name off the field in which this property

73
00:03:22,270 --> 00:03:25,050
is going to get stored again. A very good

74
00:03:25,050 --> 00:03:27,840
practice to separate the name off the

75
00:03:27,840 --> 00:03:30,970
properties in classes from the names off

76
00:03:30,970 --> 00:03:34,000
the fields in mongo D. B. But what if you

77
00:03:34,000 --> 00:03:36,490
want to exclude a particular property from

78
00:03:36,490 --> 00:03:39,140
being persisted at all? What if our model

79
00:03:39,140 --> 00:03:40,980
has some properties that it needs

80
00:03:40,980 --> 00:03:43,040
internally, but it doesn't need to

81
00:03:43,040 --> 00:03:46,550
persist? Then we can use the at transient

82
00:03:46,550 --> 00:03:49,260
annotation in this case will try to save

83
00:03:49,260 --> 00:03:52,280
objects off type aircraft idea and model

84
00:03:52,280 --> 00:03:54,400
will be persisted, but number of seats

85
00:03:54,400 --> 00:03:56,430
will not be persisted, even if it has a

86
00:03:56,430 --> 00:03:59,120
value. Also, when we dis your allies

87
00:03:59,120 --> 00:04:01,230
objects from Mongo, number of seats will

88
00:04:01,230 --> 00:04:03,610
have a default value unless otherwise

89
00:04:03,610 --> 00:04:06,450
specified. We most certainly want to

90
00:04:06,450 --> 00:04:09,520
execute filters, enquiries against Armando

91
00:04:09,520 --> 00:04:12,680
documents and to speed things up. We will

92
00:04:12,680 --> 00:04:15,510
definitely need to use index is well. In

93
00:04:15,510 --> 00:04:17,830
order to define indexes, we can use the at

94
00:04:17,830 --> 00:04:20,660
indexed annotation on the field that you

95
00:04:20,660 --> 00:04:23,370
want to index Now. In this case, let's

96
00:04:23,370 --> 00:04:25,070
assume that you want to make a lot off

97
00:04:25,070 --> 00:04:28,040
queries on the aircraft model. It makes

98
00:04:28,040 --> 00:04:30,070
sense, then tow. Annotate it with at

99
00:04:30,070 --> 00:04:32,350
indexed. And, of course, like most of the

100
00:04:32,350 --> 00:04:35,240
spring date, among connotations at indexed

101
00:04:35,240 --> 00:04:37,760
can be customized. For example, we can

102
00:04:37,760 --> 00:04:40,020
specify the direction off the index

103
00:04:40,020 --> 00:04:42,720
ascending or descending, and we can also

104
00:04:42,720 --> 00:04:46,330
specify if this field should be unique.

105
00:04:46,330 --> 00:04:49,130
Feels that are unique cannot have the same

106
00:04:49,130 --> 00:04:51,580
value in a mongo that our best collection

107
00:04:51,580 --> 00:04:54,100
you'll get an error if you try to insert

108
00:04:54,100 --> 00:04:56,370
fields that have the same value. For

109
00:04:56,370 --> 00:04:59,310
example, I D is an implicitly indexed

110
00:04:59,310 --> 00:05:02,450
field, which is also unique by default. In

111
00:05:02,450 --> 00:05:05,470
our case, we let unique equals false.

112
00:05:05,470 --> 00:05:07,980
There is also texting. Next we'll discuss

113
00:05:07,980 --> 00:05:09,860
more about its. We will implement full

114
00:05:09,860 --> 00:05:12,770
text search. Just know that if you want to

115
00:05:12,770 --> 00:05:15,820
include feels in a foot X search, then you

116
00:05:15,820 --> 00:05:18,070
need to annotate them with at text

117
00:05:18,070 --> 00:05:20,760
indexed. In this case, model will be

118
00:05:20,760 --> 00:05:23,070
included in a full deck search. And, of

119
00:05:23,070 --> 00:05:25,540
course, you can text index as many fields

120
00:05:25,540 --> 00:05:28,540
as you want. At Compound Index allows us

121
00:05:28,540 --> 00:05:31,760
to create an indexed from multiple fields

122
00:05:31,760 --> 00:05:34,130
within our document. Instead of creating

123
00:05:34,130 --> 00:05:36,510
an index for each field, we have a single

124
00:05:36,510 --> 00:05:39,100
index, which is linked to multiple

125
00:05:39,100 --> 00:05:41,470
properties. This is great for optimization

126
00:05:41,470 --> 00:05:44,330
Sze. In this case, our compound index is

127
00:05:44,330 --> 00:05:47,190
based on the model in an ascending manner.

128
00:05:47,190 --> 00:05:50,070
Notice the one over there and a number of

129
00:05:50,070 --> 00:05:52,890
seats in descending notes. The minus one.

130
00:05:52,890 --> 00:05:55,050
Over here, it's a shorthand notation for

131
00:05:55,050 --> 00:05:58,230
ascending and descending. Like I said,

132
00:05:58,230 --> 00:06:01,540
pretty useful for optimization Sze. And

133
00:06:01,540 --> 00:06:04,530
finally we have at the B ref. If you're

134
00:06:04,530 --> 00:06:07,590
familiar with relational databases at tbe,

135
00:06:07,590 --> 00:06:10,900
ref acts someone like a joint in our

136
00:06:10,900 --> 00:06:14,460
particular case, a debrief is used to link

137
00:06:14,460 --> 00:06:17,590
multiple documents Together. We have a

138
00:06:17,590 --> 00:06:20,450
manufacturer property and his property is

139
00:06:20,450 --> 00:06:22,850
off type manufacturer. But you do not want

140
00:06:22,850 --> 00:06:25,010
to start a manufacturer as part of the

141
00:06:25,010 --> 00:06:27,280
aircraft document. Instead, we want to

142
00:06:27,280 --> 00:06:29,640
link it to an existing document. That's

143
00:06:29,640 --> 00:06:32,170
what added the brief is all about. It

144
00:06:32,170 --> 00:06:35,700
thinks the value over here toe on existing

145
00:06:35,700 --> 00:06:38,510
document in a different collection. And

146
00:06:38,510 --> 00:06:40,020
when we look at the right at the

147
00:06:40,020 --> 00:06:42,080
manufacturer, we can see that it's a

148
00:06:42,080 --> 00:06:43,730
completely different document with its own

149
00:06:43,730 --> 00:06:47,350
I D and its own properties. Relationships

150
00:06:47,350 --> 00:06:50,360
between documents are very important and

151
00:06:50,360 --> 00:06:55,000
interesting topic, and we'll discuss more about them in a future module.

