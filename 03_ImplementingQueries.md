# 1. Mongo Query Object

- Mongo Query definition example
    - filter
    - sort
    - paging
```java
Query byAge = Query
        .query(Criteria.where("age").gt(18))
        .with(Sort.by(Sort.Direction.DESC, "age"))
        .with(PageRequest.of(1,10));
```

- Mongo Filter Operators
    - is/ne
    - lt/lte
    - gt/gte
    - in
    - exists
    - regex
    
- `MongoTemplate` is the component that executes 
requests to MongoDB
  - a simple injectable class following the standard
    template pattern in Spring
  - It allows us to perform CRUD operations on data
  - It allows us to execute commands against a MongoDB
  - Low level


# 2. Creating and Executing Queries

```java
Query byAge = Query
        .query(Criteria.where("age").gt(18))
        .with(Sort.by(Sort.Direction.DESC, "age"))
        .with(PageRequest.of(1,10));

List<Person> people = this.mongoTemplate.find(byAge, Person.class);
```

- Featching data with Mongo Template
  - The query definition with filters, sorts, ... etc
  - Decide the outcome of the query
  - Class type that contains Mongo annotation

```java
// Define a query, inject the template
MongoTemplate template = ....
Query q = ...

// fetch all persons
template.findAll(Person.class);

// fetch all person matching query
template.find(q, Person.class );

// fetch a single person matching query
template.findOne(q, Person.class)

// count all people matching query
template.count(q, Person.class)
```

- Example with `Aircraft` Class:
```java
@Document
class Aircraft {
    @Id String id;
    @Indexed(unique=true) private String code;
    private String farmily;
    private int nvSeats;
    private Engine engine;
}

class Engine {
    private boolean needsMaintenance;
}
```

  - Find all document
    ```java
    List<Aircraft> res = mongoTemplate.findAll(Aircraft.class);
    // Or
    Query allAircraft = new Query();
    List<Aircraft> res = mongoTemplate.find(allAircraft, Aircraft.class );
    ```
  
  - Filter with multiple operators
    ```java
    Query nbSeatsBetween = Query.query(
        Criteria.where("nbSeats")
        .gt(100)
        .lt(300)
    );
    
    List<Aircraft> res = mongoTemplate.find(nbSeatsBetween, Aircraft.class );
    ```
  -  Multiple Criteria: `Or`
     ```java
     Query bigAricraftOr737 = Query.query(
        new Criteria().orOperator(
            Criteria.where("farmily").is("737"),
            Criteria.where("nbSeats").is(250),
        )
     );
     List<Aircraft> res = mongoTemplate.find(bigAricraftOr737, Aircraft.class );
     ```
  -  Multiple Criteria: `Or`
     ```java
     Query bigAricraftOr737 = Query.query(
        new Criteria().andOperator(
            Criteria.where("farmily").is("737"),
            Criteria.where("nbSeats").is(250),
        )
     );
     List<Aircraft> res = mongoTemplate.find(bigAricraftOr737, Aircraft.class );
     ```
     
  -  Filter Subdocuments
      ```java
      // engine is a property of Aircraft
      // needsMaintenance is a property if Engine (bool)
      Query engineNeedingMaintenance = Query.query(
        Criteria.where("engine.needsMaintenance").is(true)
      );
     
      List<Aircraft> res = mongoTemplate.find(
        engineNeedingMaintenance, Aircraft.class   
      );
      ```
  - Sorting the result
    ```java
    Query moreThan100Seats = Query.query(
        Criteria.where("nbSeats").gt(100)
    ).with(
        Sort.by(Sort.Direction.ASC, "nbSeats")
    );
    
    List<Aircraft> res = mongoTemplate.find(moreThan100Seats, Aircraft.class );
    ```
  - Paging the result
    ```java
    Query moreThan100Seats = Query.query(
    Criteria.where("nbSeats").gt(100)
    ).with(
    Sort.by(Sort.Direction.ASC, "nbSeats")
    ).with(
      PageRequest.of(
      1, // page number
      20 // page size
      )
    );

    List<Aircraft> res = mongoTemplate.find(moreThan100Seats, Aircraft.class );
    ```
# 3. Implementing full text search
   
- Text  Indexes
    - Work on properties of type string or arrays of string
    - You can text index properties across the whole object graph
    - Pay attention to weights as they may change the order
      or relevance of a found document 
      
- Annotations
```java
@Document
class Profile {
    @Id private String id;
    private String name;
    @TexIndexed private String title;
    @TexIndexed(weight=2) private String aboutMe ;
}
```

- Executing a full text search
```java
TextCriteria textCriteria = TextCriteria
    .forDefaultLanguage()
    .matching("test");

Query byFreeText = TextQuery
        .queryText(textCriteria)
        .sortByScore()  // internal score computed by Mongo
        .with(PageRequest.of(0, 3));

return mongoTemplate.find(byFreeText, FlightInformation.class ); 
```



# 4. Demo
# 5. Recap