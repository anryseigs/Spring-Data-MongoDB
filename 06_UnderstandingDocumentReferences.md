# 1. Mongo Document References Explained

- A way to link togerther multiple documents that
are related.
  - for most cases, the  denormalized model where data is store in a single document
    is optimal

- Example (Bank transaction)
```Json
{
  "_id": 1,
  "amount": 3000,
  "customer": {
    "name": "John Doe",
    "nationalIdNb": "Az456734"
  }
},
{
  "_id": 2,
  "amount": 1000,
  "customer": {
    "name": "John Doe",
    "nationalIdNb": "Az456734"
  }        
}
```
-> duplicate customer data
=> we need link customerID in `customer` document 

- 2 way to link
    - Manual: save the "_id" of `customer` document to field *customerId* in `transaction` document
    - DBRefs: link documents together by using the "_id" field, collection name and database name.
        - Can link documents across collections or across different databases
        - to resolve DBRefs, the application must perform additional queries.
        - Cascading is not supported 

- DBRef Format
    - `$ref` : the name of the collection where the linked document resides
    - `$id`: the value of the "_id" field of referenced document
    - `$db`: yje name of database where the referenced docuemnt resides
    ```json
    {
        "_id": ObjectId("xxx-xxx-xx-xxx-xxxxxx"),
        "name": 737,
        "capacity": 234,
        "engine": {
            "$ref": "engines",
            "$id": ObjectId("xxxxxxx-xxxxx-xxxx"),
            "$db": "atm"    // optional
        }
    }
    ```    
    - The order of fields in DBRef matters and must be used in the correct sequence.
    - DBRefs are not "SMART"
    
# 2. Creating Document References in Spring Using @DbRef

- origin data
```java
@Document
public class Aircraft {
    @Id private String id;
    private String model;
    private int maxPassengers;
    @DBRef    private Engine engine;   // document reference
}


@Document
public class Engine {
    @Id private String id;
    private String type;
    private int maxPower; 
}
```

- Spring Data MongoDB fetches documents annotated with `@DBRef` for us automatically.

- if you run
    ```java
    Aircarft a = new Aircarft("737");
    a.setEngine(new Engine("GM", 1000));
    
    aircraftRepository.insert(a);
    ```
  
    - You won't get an error
    - But the engine is not getting saved in the Engine collection
    - Beacause Cascading does not work with DBRefs on save by default 

=> correct way 
    
```java
Engine e = new Engine("GM", 1000);
engineRepository.insert(e);

Aircraft a = new Aircraft("737");
a.setEngine(e);
aircraftRepository.insert(a);
```

- Lazy loading
    ```java
    @Document
    public class Aircraft {
    @Id private String id;
    @DBRef(lazy=true) private Engine engine;
    }
    ```
  - by default, `@DBRef` instructs the framework to perform an eager load
  - This can be changed by modifying the lazy property to "true"
    - the sub-document is loaded when one of its fields/methods is accessed
    - useful for bi-directional references to avoid a cyclic dependency.
    
- NOTE filter with DBRef
    - will work:
        ```java
        @Document
        public class Aircraft {
            @Id private String id;  
            private Engine engine;
        }
      
        // Will work
        @Query("{''engine.maxPower}: ?0")
        List<Aircraft> byPower(int pow);
        ```
    - won't work
        ```java
        @Document
        public class Aircraft {
            @Id private String id;  
            @DBRef private Engine engine;
        }
      
        // Will work
        @Query("{''engine.maxPower}: ?0")
        List<Aircraft> byPower(int pow);
        ```
# 3. Spring Mongo Lifecycle Events

- Events that your application can repond to by registering special beans in the Spring
application context.
  
- `Before` event:
    - `onBeforeConvert`
    - `onBeforeSave`
    - `onBeforeDelete`
- `After` events:
    - `onAfterConvert`
    - `onAfterSave`
    - `onAfterLoad`
    - `onAfterDelete`

- Save/Update document events
    - 1. call insert or save in `MongoTemplate`
    - 2. `onBeforeConvert` is called before the Java object is converted to a 
    Document by the MongoConverter
    - 3. `onBeforeSave` is called before inserting or saving the document
    in the database
    - 4. `onAfterSave` is called after the document was inserted or saved in 
    the database

- Load Document Event
    - 1. Call find, findOne, findAndRemove in MongoTemplate
    - 2. `onAfterLoad` is called after the Document has been retrieved from
    the database
    - 3. `onAfterConvert` is called after the Document has been converted into
    a POJO
         
- Delete Documents Events
    - 1. Call remove on MongoTemplate
    - 2. `onBeforeDelete` is called just before the document
    gets deleted from database
    - 3. `onAfterDelete` is called after the Document has been 
    deleted from the database
         
- Event Behavior
    - Lifecycle events are emitted only for root level types. 
    Sub-documents are not subject to event publication unless they are
      annotated with `@DBRef`
    - It is all happening in an async fashion. We have no guarantees to when an event
    is processed.

- Why would i ever want to hook into these events ?
    - Use cases:
        - implement cascade on save
        - trigger some job/acion in different systems
        - Security audits
- How can i hook into these event and execute my custom logic ?
    - example:
        - `AbstractMongoEventListener.java`
            ```java
            public abstract class AbstractMongoEventListener<E> 
                implements ApplicationListener<MongoMappingEvent<?>> {
          
                public void onAfterConvert(AfterConvertEvent<E> event) {...}
                public void onAfterDelete(AfterDeleteEvent<E> event) {...}
                public void onAfterLoad(AfterLoadEvent<E> event) {...}
                
                // ....
            }
            ```
        - `BeforeSaveListener.java`  
            ```java
            @Component
            public class BeforeConvertListener extends 
                AbstractMongoEventListener<Flight> {
                
                @Override 
                public void onBeforeConvert(BeforeConvertEvent<Flight> event) {
                    // your custom logic
                }
            }
            ```
    - Create an event listener per feature
# 4. Demo