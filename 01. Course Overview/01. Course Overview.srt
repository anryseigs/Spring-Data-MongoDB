1
00:00:06,800 --> 00:00:08,040
[Autogenerated] Hi, everyone, and welcome

2
00:00:08,040 --> 00:00:11,010
to my course Spring Framework Spring Data

3
00:00:11,010 --> 00:00:13,670
Model Baby. My name is Daniel Boone, and

4
00:00:13,670 --> 00:00:15,970
I'm a passionate, softer developer, clean

5
00:00:15,970 --> 00:00:18,730
code enthusiast and blogger. We can find

6
00:00:18,730 --> 00:00:22,050
you a line on Twitter at Romanian coder or

7
00:00:22,050 --> 00:00:25,430
or my blogged Romanian coder dot com have

8
00:00:25,430 --> 00:00:27,600
been working with Monk OTB for the past

9
00:00:27,600 --> 00:00:30,410
seven years and used it with all the major

10
00:00:30,410 --> 00:00:32,970
technology stocks, including java, dot net

11
00:00:32,970 --> 00:00:35,620
and Note Mom, Would it be is a trending

12
00:00:35,620 --> 00:00:38,490
technology. In fact, it occupies the fifth

13
00:00:38,490 --> 00:00:41,480
position in that village. This course is

14
00:00:41,480 --> 00:00:43,540
not about becoming proficient using nobody

15
00:00:43,540 --> 00:00:46,990
be in enterprise spring applications. Some

16
00:00:46,990 --> 00:00:49,140
of the major topics that will cover

17
00:00:49,140 --> 00:00:51,580
include connecting a spring application

18
00:00:51,580 --> 00:00:53,940
removal database, implemented quite

19
00:00:53,940 --> 00:00:56,720
operations and using mobile repositories,

20
00:00:56,720 --> 00:00:59,420
managing relationships between documents,

21
00:00:59,420 --> 00:01:01,690
handling that immigrations and adding

22
00:01:01,690 --> 00:01:03,760
integration tests for the Mongo data.

23
00:01:03,760 --> 00:01:06,130
Access left by you know discourse. You

24
00:01:06,130 --> 00:01:08,510
learn how to tackle all aspects off their

25
00:01:08,510 --> 00:01:10,560
persistence using spring framework, and

26
00:01:10,560 --> 00:01:13,070
nobody be. In fact, you'll be able to use

27
00:01:13,070 --> 00:01:15,630
your skills in real project right away.

28
00:01:15,630 --> 00:01:17,460
Before beginning his course. You should be

29
00:01:17,460 --> 00:01:20,440
familiar with my body be Java and spring

30
00:01:20,440 --> 00:01:22,510
framework. Some basic knowledge of spring

31
00:01:22,510 --> 00:01:24,950
Boot and J Unit will be a plus, but it's

32
00:01:24,950 --> 00:01:27,340
not mandatory. I hope you'll join me on

33
00:01:27,340 --> 00:01:29,570
this journey to integrate mobile TB in

34
00:01:29,570 --> 00:01:32,030
enterprise spring applications with a

35
00:01:32,030 --> 00:01:41,000
Spring Framework Spring Data Manga TV course at Pro Sight.

