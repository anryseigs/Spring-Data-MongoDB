# 1. Insert new document
- Insert single
```java
Person p = new Person("test", 12);
mongoTemplate.insert(p);
```

- Combine insert
```java
Person a = new Person("A", 12);
Person b = new Person("B", 12);
Person c = new Person("B", 12);

List<Person> people = Arrays.asList(a, b, c);
mongoTemplate.insertAll(people);
```

# 2. Updating document
- save method == insert + update
    - scan collection to find by ID
    - if no document is found for the given ID => save like Insert.
    A new document is creted with the provided ID
    - If a document is found, replaced with the provided one
    - if no ID - generates one and insert document
    
- only Single update
```java
Airport opt = mongoTemplate.findById("xxxxx");

opt.setFlightsPerDay(3000);
opt.setProximityWeather(Weather.RAIN);

mongoTemplate.save(opt);
```
    - Note: dont use save for insert. 

- Batch updates
```java
Query airportsInRomania = Query.query(
        Criteria.where("country").is("Romania")
        );

Update setWeather = Update.update("proximityWeather", Weathr.CLOUDS);

mongoTemplate.updateMulti(
        airportsInRomania,  // filter
        setWeather,         // update value
        Airport.class
        );

// or update First
mongoTemplate.updateFirst(
        airportsInRomania,  // filter
        setWeather,         // update value
        Airport.class
        );
```
# 3. Deleting document

- Delete single:
```java
Airport instanbul = ... //fetch from database
mongoTemplate.remove(istanbull)
```

- Delete many

```java
Query franceAirport = Query.query(
        Criteria.where("country").is("France")
        );

mongoTemplate.findAllAndRemove(franceAirport, Airport.class );
```

- delete all

```java
Query queryAll = Query.query();
mongoTemplate.findAllAndRemove(queryAll, Airport.class);
```

- drop collection
```java
mongoTemplate.fropCollection(Airport.class);
```
# 4. Using Mongo Custom Converters

- `Mongo Converter` is a feature used for mapping all Java
types to/from DBObjects when storing or retrieving these objects.
  
- Type Conversion FROM `Java` TO `MongoDB`
    - UUID/String   => ObjectID
    - String        => String
    - int           => Number
    - double        => Number
    - boolean       => Boolean
    - Object        => Object (embedded)
    
- Mono Converter Creation Process 
    - Create write converter (from Java type to Mongo type)
    - Create read converter (from Mongo type to Java type)
    - Register converters as a Spring bean
    
- Example
```java
@Document 
class Person {
    @Id private String id;
    private String name;
    private Address address;
}

class Address {
    private String city;
    private String country;  
}
```

Origin MongoDB data
```json
{
  "_id": "test_id",
  "name": "test_name",
  "address": {
    "city": "test_city",
    "country": "test_country"
  }
}
```
=> convert to Java object
```json
{
  "_id": "test_id",
  "name": "test_name",
  "address": "test_city,test_country"
}
```

- Implement the Mongo Converters

    - `AddrWriteConverter.java`
        ```java
        import org.springframework.core.convert.converter.Converter;
      
        public class AddrWriteConverter implements Converter<Address, String> {
            @Override 
            public String convert(Address address) {
                return address.getCity() + "," + address.getCountry();
            }
        }
        ```
    - `AddrReadConverter.java`
        ```java
        import org.springframework.core.convert.converter.Converter;
      
        public class AddrReadConverter implements Converter<String, Address> {
            @Override 
            public Address convert(String s) {
                String[] parts = s.split(",");
                return new Address(s[0], s[1]);
            }     
        }
        ```
    - `AirportApplication,java`
        ```java
        @Bean
        public MongoCustomConversions customConversions() {
            List<Convert<?, ?>> converters = new ArrayList<>();
            converters.add(new AddrReadConverter());
            converters.add(new AddrWriteConverter());
            return new MongoCustomConversions(converters);
        }
        ```
        - you can register the converters in any `@Configuration` class.
        In Spring Boot, it can be the main application class; else define a new class 
          and annotate it with `@Configuration`


# 5. Demo