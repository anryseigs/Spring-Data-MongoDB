1
00:00:00,700 --> 00:00:01,780
[Autogenerated] I'm very happy to be the

2
00:00:01,780 --> 00:00:04,700
demo because now I can get rid of the poor

3
00:00:04,700 --> 00:00:07,210
man's migration components that I have so

4
00:00:07,210 --> 00:00:10,000
far and create a proper solution in his

5
00:00:10,000 --> 00:00:11,760
demo will be implementing that

6
00:00:11,760 --> 00:00:13,750
immigration's in spring applications using

7
00:00:13,750 --> 00:00:16,120
mongo, Be well configuring will be and

8
00:00:16,120 --> 00:00:18,250
they will implement migration using change

9
00:00:18,250 --> 00:00:21,730
logs and Jane sets before we can start

10
00:00:21,730 --> 00:00:24,020
using my Uncle B. We need Toe added as a

11
00:00:24,020 --> 00:00:26,570
dependency. I opened the bomb that XML

12
00:00:26,570 --> 00:00:30,090
file and here we need to add in the Mongol

13
00:00:30,090 --> 00:00:32,750
be dependency. I'm going to use these

14
00:00:32,750 --> 00:00:36,140
snippets and have the Mongol be dependency

15
00:00:36,140 --> 00:00:38,810
in place. The group parties calm that kid

16
00:00:38,810 --> 00:00:41,010
have them will be and the artifact is

17
00:00:41,010 --> 00:00:43,060
Mongol. Be as of today, the current

18
00:00:43,060 --> 00:00:45,910
version 0 13 But it's can change in the

19
00:00:45,910 --> 00:00:49,340
future. Now we can define the Mongol be

20
00:00:49,340 --> 00:00:51,760
being I'm going to define the being and

21
00:00:51,760 --> 00:00:54,000
the Airport Management Application Class,

22
00:00:54,000 --> 00:00:56,150
which is the main entry point into a

23
00:00:56,150 --> 00:00:58,640
speedboat application. The big definition

24
00:00:58,640 --> 00:01:00,730
is participle to create a new method which

25
00:01:00,730 --> 00:01:03,210
returns an instance of Mongo B and we

26
00:01:03,210 --> 00:01:05,780
annotated with Al Bean. We grabbed two

27
00:01:05,780 --> 00:01:07,690
configuration items from the Application

28
00:01:07,690 --> 00:01:11,020
Properties file, the Mongol, you are I and

29
00:01:11,020 --> 00:01:14,440
the migrations are enabled global. Then we

30
00:01:14,440 --> 00:01:16,720
create a new Mongol. Be runner by passing

31
00:01:16,720 --> 00:01:19,720
in the Mongol your eye and we go ahead to

32
00:01:19,720 --> 00:01:22,760
configure our Mongol be business. We use

33
00:01:22,760 --> 00:01:25,290
the setting able method to define if the

34
00:01:25,290 --> 00:01:28,620
runner should be enabled or not. Then we

35
00:01:28,620 --> 00:01:31,580
set the change Log scan package, toe

36
00:01:31,580 --> 00:01:34,360
parasite, airport management, The bee that

37
00:01:34,360 --> 00:01:37,270
migrations All off archangel files are

38
00:01:37,270 --> 00:01:39,380
going to be defined in that package. In

39
00:01:39,380 --> 00:01:41,560
fact, it's a good practice to group your

40
00:01:41,560 --> 00:01:44,990
migration files in a single place. Then

41
00:01:44,990 --> 00:01:46,920
we're setting the cheese log collection,

42
00:01:46,920 --> 00:01:51,260
name and lock collection names. Now we're

43
00:01:51,260 --> 00:01:54,090
passing in the Mongol template. We want to

44
00:01:54,090 --> 00:01:56,750
be able to use mobile template inside the

45
00:01:56,750 --> 00:01:59,510
chain, said methods, So we have to set it

46
00:01:59,510 --> 00:02:02,200
here globally. Were you returned Runner

47
00:02:02,200 --> 00:02:04,130
and we're done. It's worth mentioning that

48
00:02:04,130 --> 00:02:06,850
you can declare these mongo be being in

49
00:02:06,850 --> 00:02:11,210
any class annotated with ad configuration.

50
00:02:11,210 --> 00:02:13,240
I'm going to create our first log under

51
00:02:13,240 --> 00:02:16,060
the migrations package. We're creating a

52
00:02:16,060 --> 00:02:18,460
class called Debbie Change log 001 and we

53
00:02:18,460 --> 00:02:20,850
have the earth changed. Look, annotation

54
00:02:20,850 --> 00:02:23,880
because I want to see data in this class.

55
00:02:23,880 --> 00:02:26,740
The order has to be 001 I want this

56
00:02:26,740 --> 00:02:28,940
change. Look to be the 1st 1 that gets

57
00:02:28,940 --> 00:02:32,060
executed, and then let's go ahead with the

58
00:02:32,060 --> 00:02:36,260
implementation. The implementation should

59
00:02:36,260 --> 00:02:39,050
be very familiar. The first, Chin said, is

60
00:02:39,050 --> 00:02:41,650
going to see all the airports. Note the

61
00:02:41,650 --> 00:02:43,930
chain, said annotation. It's the first

62
00:02:43,930 --> 00:02:46,320
thing you said, because it has order equal

63
00:02:46,320 --> 00:02:49,420
to 001 It has an I. D. C. The airport, and

64
00:02:49,420 --> 00:02:53,890
he has me as an author. The method itself

65
00:02:53,890 --> 00:02:56,010
creates a couple of airports and then uses

66
00:02:56,010 --> 00:02:57,990
the Mongol template to instead, Um,

67
00:02:57,990 --> 00:03:00,250
remember, we can use the Mongol template

68
00:03:00,250 --> 00:03:01,980
is had the chain said, because you defined

69
00:03:01,980 --> 00:03:06,030
it globally in the being. After that, we

70
00:03:06,030 --> 00:03:08,890
can also see the flights. You're creating

71
00:03:08,890 --> 00:03:10,700
some flights and they were using the

72
00:03:10,700 --> 00:03:12,960
insert old method on the model template,

73
00:03:12,960 --> 00:03:15,200
and that's it. Let's go ahead and run this

74
00:03:15,200 --> 00:03:20,400
application and again see my gracious have

75
00:03:20,400 --> 00:03:23,230
been executed. Also get all the flight

76
00:03:23,230 --> 00:03:27,690
information in our collection Awesome.

77
00:03:27,690 --> 00:03:29,910
Let's assume that the name of an existing

78
00:03:29,910 --> 00:03:31,590
airport like room is going to change in

79
00:03:31,590 --> 00:03:34,770
the future. For that, we can create a new

80
00:03:34,770 --> 00:03:36,890
migration. Barry's in jeans logs. We

81
00:03:36,890 --> 00:03:38,750
define a new class. Call it d be changed.

82
00:03:38,750 --> 00:03:41,290
Logs original, too. We add the actual ____

83
00:03:41,290 --> 00:03:44,150
annotation with the order 002 so it's

84
00:03:44,150 --> 00:03:46,440
going to get executed after the initial

85
00:03:46,440 --> 00:03:50,270
sitting. The implementation should also

86
00:03:50,270 --> 00:03:52,830
feel familiar. We have a new method called

87
00:03:52,830 --> 00:03:55,050
Update from Airport. We're passing in the

88
00:03:55,050 --> 00:03:57,520
mongo template. Everything that at Chain

89
00:03:57,520 --> 00:04:00,270
said annotation, is the first set of

90
00:04:00,270 --> 00:04:02,820
reaching block. We're grabbing the Rome

91
00:04:02,820 --> 00:04:04,990
airport, and we're updating the name using

92
00:04:04,990 --> 00:04:08,920
the set name method. They were saving it.

93
00:04:08,920 --> 00:04:11,270
And thats on migration. Let's for the

94
00:04:11,270 --> 00:04:14,150
application again and see if the migration

95
00:04:14,150 --> 00:04:17,260
is correctly applied. And here in the

96
00:04:17,260 --> 00:04:19,170
logs, we can see that only the change log

97
00:04:19,170 --> 00:04:22,610
002 and Jane said Sierra 01 have bean

98
00:04:22,610 --> 00:04:25,640
applied the Rome airport as the new name

99
00:04:25,640 --> 00:04:28,260
updated, which is great. So Mongol be

100
00:04:28,260 --> 00:04:30,900
automatically executed. Only the needed

101
00:04:30,900 --> 00:04:34,140
change logs. In fact, let's go ahead and

102
00:04:34,140 --> 00:04:37,840
look at the data base structure. I fired

103
00:04:37,840 --> 00:04:41,210
up Mongol to be compass, and we have the

104
00:04:41,210 --> 00:04:43,680
Airport Management M seven collection, and

105
00:04:43,680 --> 00:04:45,400
we have airports and flights which are the

106
00:04:45,400 --> 00:04:47,540
usual collections. And then we have two

107
00:04:47,540 --> 00:04:49,680
additional collections called migrations

108
00:04:49,680 --> 00:04:52,190
and migrations lock. These collections are

109
00:04:52,190 --> 00:04:54,560
used by Manco. Be internally. Remember

110
00:04:54,560 --> 00:04:56,980
that We did find them when we created the

111
00:04:56,980 --> 00:05:01,600
Mongol. Be meat. If you open the

112
00:05:01,600 --> 00:05:04,840
migrations collection, we can see all the

113
00:05:04,840 --> 00:05:07,350
migrations that have been executed against

114
00:05:07,350 --> 00:05:10,310
this database. The airport seed, the

115
00:05:10,310 --> 00:05:13,640
flight seed and the average Rome airport.

116
00:05:13,640 --> 00:05:15,750
This is how mongo be keeps track

117
00:05:15,750 --> 00:05:17,600
internally of the migrations that have

118
00:05:17,600 --> 00:05:23,000
been applied. You can consider this. Ah, migration put it.

