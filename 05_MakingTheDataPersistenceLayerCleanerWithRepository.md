# 1. Discover Spring Repositories
- Spring (Mongo) Repository: 
  - Abstraction (interface) that significantly reduces the amount of boilerplate code need 
    to implement the data access layer.
  - Repository Properties
    - An injectable interface that can be used in Spring applications
    - Provide basic CRUD operations capabilities out of the box
    - can be expanded with ease

- Mongo Repository Interface 
    - Spring Data
        - Repository<T, ID>
        - CrudRepository<T, ID>:
            - save, saveAll, findAll, findByID,
        existsById, count, deleteById, deleteAll
        - PagingAndSortingRepository<T, ID>
            - findAll(Sort s), findAll(Pageable p)
    - Mongo 
        - MongoRepository<T, ID>
            - insert(T o), insert(Iterable<T> list)

- Example
    - `Airport.java`
        ```java
        @Document
        public class Airport {
          @Id String id;
          String country;
          int nbFlightPerDay;
          //....
        }
        ```
    - `AirportRepository.java`
        ```java
        @Repository
        public interface AirportRepository extends MonoRepository<Airport, String> {
        }
        ```
    - `AirportController.java`
        ```java
        List<Airport> airport = repository.findAll();
        
        Airport a = new Airport("London", 400);
        repository.insert(a);
        repository.deleteAll();
        ```
- NOTE: For each repository interface, Spring registers the technology specific factory
bean to create the appropriate proxy implementations.

# 2. Query Methods

- declarative way to add functionality to a Spring repository.

- Example
```java
@Document
public class Airport {
    @Id String id;
    int flightPerDay;
    String name;
    boolean closed;
    Location location;
}

public class Location {
    String city;
    String country;
    Spring email;
}
```

  - Add a new query method
    ```java
    @Repository
    public interface AirportRepository extends MongoRepository<Airport, String> {
        List<Airport> findByFlightsPerDayGreaterThan(int value);
    }
    
    // Call the method, implementation will be taken care of the proxy
    List<Airport> airports = repository.findByFlightsPerDayGreaterThan(200); 
     ```

- The Spring Framework knows how to create a proxy based on the query method signatures.

- How to build query methods:
    - return type
    - method Prefix (findBy)
    - Property Name
    - Filter(s)

- Build query methods for numeric Properties  
```java
List<Airport> findByFlightPerDayBetween(int min, int max);
    // return type: List<Airport>
    // method prefix findBy
    // property name: FlightPerDay
    // Filter Between(int min, int max)

List<Airport> findByFlightPerDayGreaterThan(int value);
List<Airport> findByFlightPerDayGreaterThanEqual(int value);
List<Airport> findByFlightPerDayLessThanEqual(int value);
```

- Build Query Methods for Spring properties
```java
List<Airport> findByNameLike(String airportName);
List<Airport> findByNameNotNull();
List<Airport> findByNameNull();

Optional<Airport> findByName(String aiportName);
Airport findByName(String airportName);
```

- Build Query methods for Boolean Properties
```java
List<Airport> findByClosedTrue();
List<Airport> findByClosedFalse();
```

- Build Complex query methods
```java
// The And operator can be used to combine multiple filter
List<Airport> findByClosedTrueAndFlightsPerDayGreaterThan(int minFlights);
```

- With more complex query, you can also make repository methods execute
custom Mongo Query Language constructs
```java
@Repository
public interface AirportRepository extends MongoRepository<Airport, String> {
    @Query("{'location.city': ?0}")         // ?0 == first argument
    List<Aiport> findByCity(String city);   // method name not relevant
    
    @Query("{'flightsPerDay': {$lte: 50}}")
    List<Airport> findSmallAirports();
}
```

- Prefer standard query methods instead of `@Query` where possible.
`@Query` just for more complex scenarios.
 
# 3. Insert, Update, Delete Document

- Repository interfaces take care of inserts, updates, and deletes

- Build-in Methods
    - insert
    - save
    - delete
    - deleteAll 
    
```java
@Repository
public interface AirportRepository extends MonoRepository<Airport, String> {
    
}

// insert single
Airport a = new Airport("Madrid", 250);
repository.insert(a);

// insert batch
Airport a1 = new Airport("Madrid1", 250);
Airport a2 = new Airport("Madrid2", 250);
List<Airport> airports = Arrays.asList(a1, a2);

repository.insert(airports)

// update
Airport a = repository.findById("test_id");     // find document first
a.setFlightsPerDay(300);                        // set new value
repository.save(a);                             // update
 
// delete a document
Airport a = repository.findById("test_id");     // find document first
repository.delete(a);                           // delete it

// or delete by ID
Airport a = repository.deleteById("test_id");

// delete all
repository.deleteAll();
```
# 4. Demo